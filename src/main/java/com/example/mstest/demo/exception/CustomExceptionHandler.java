package com.example.mstest.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAllException(Exception e, WebRequest req){
        ExceptionResponse resp= new ExceptionResponse();
        resp.setMessage("Error occurred");
        resp.setDetail(e.getCause().toString());
        return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<Object> handlerProductNotFound(ProductNotFoundException e, WebRequest req){
        ExceptionResponse resp= new ExceptionResponse();
        resp.setDetail("Product Not Found");
        resp.setMessage(e.getCause().toString());
        return new ResponseEntity<>(resp, HttpStatus.NOT_FOUND);
    }

}
