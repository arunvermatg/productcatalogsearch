package com.example.mstest.demo.dao;

import com.example.mstest.demo.exception.ProductNotFoundException;
import com.example.mstest.demo.model.Product;

import java.util.List;

public interface ProductSearchDao {
    public List<Product> getAllProducts();
    public Product getProductBySku(String sku) throws ProductNotFoundException;

    public List<Product> productByBrand(String brand);
}
