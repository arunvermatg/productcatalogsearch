package com.example.mstest.demo.dao;

import com.example.mstest.demo.exception.ProductNotFoundException;
import com.example.mstest.demo.model.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Repository
public class ProductSearchDaoImpl implements ProductSearchDao {

    private static CopyOnWriteArrayList<Product> listOfProducts= new CopyOnWriteArrayList<>();
    static{
        for(int i=0;i<10;i++){
            Product product= new Product.Builder().name("Pant").brand(Brand.AMERICAN_EAGLE)
                    .description("This is a full pant")
                    .sku("PNT00"+i)
                    .color(ProdColor.BLACK).build();
            double basePrice= Price.getPrice();
            double taxablePrice= basePrice*0.18;
            Price price= new Price(basePrice, taxablePrice);
            product.setPrice(price);

            ProductCategory category= new ProductCategory("Pant", CategoryType.BOTTOM,"Amazon");
            product.setCategory(category);
            listOfProducts.add(product);

        }

        for(int i=11;i<20;i++){
            Product product= new Product.Builder().name("Shirt").brand(Brand.VAN_HEUSEN)
                    .description("This is a full shirt").price(new Price(Price.getPrice(), Price.getPrice()*0.18))
                    .sku("SHR00"+i).build();
            ProductCategory category= new ProductCategory("Top",CategoryType.TOP, "Flipkart");
            product.setCategory(category);
            listOfProducts.add(product);

        }
    }
    @Override
    public List<Product> getAllProducts() {
        return listOfProducts;
    }

    @Override
    public Product getProductBySku(String sku) throws ProductNotFoundException {
       Optional<Product> prod=listOfProducts.stream().filter(x->x.getSku().equals(sku)).findFirst();
       if(prod.isPresent()){
           return prod.get();
       }else{
           throw new ProductNotFoundException("Product is not available");
       }

    }


    public List<Product> productByBrand(String brand){
       Map<Brand, List<Product>> prods= listOfProducts.stream().collect(Collectors.groupingBy(Product::getBrand));
      List<Product> prd= prods.entrySet().stream().filter(x->x.getKey().toString().equals(brand)).flatMap(x->x.getValue().stream()).collect(Collectors.toList());
      return prd;
    }
}
