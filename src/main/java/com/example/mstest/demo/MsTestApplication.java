package com.example.mstest.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableHystrix
public class MsTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsTestApplication.class, args);
    }

}
