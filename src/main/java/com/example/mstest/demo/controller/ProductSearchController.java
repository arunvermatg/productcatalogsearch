package com.example.mstest.demo.controller;

import com.example.mstest.demo.exception.ExceptionResponse;
import com.example.mstest.demo.exception.ProductNotFoundException;
import com.example.mstest.demo.model.Product;
import com.example.mstest.demo.service.ProductSearchService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class ProductSearchController {

    @Autowired
    private ProductSearchService service;

    @GetMapping("/products")
    public List<EntityModel<Product>> getAllProducts() throws ProductNotFoundException {
        List<Product> products= service.getAllProducts();
        List<EntityModel<Product>> entityModels= new ArrayList<>();
        for(Product product: products){
            EntityModel<Product> model= new EntityModel<>(product);
            WebMvcLinkBuilder link= linkTo(methodOn(ProductSearchController.class).getAllProducts());
            model.add(link.withSelfRel());

            Link link1= linkTo(methodOn(ProductSearchController.class).getProductBySku(product.getSku())).withRel("product-by-sku");
            model.add(link1);

            Link link2= linkTo(methodOn(ProductSearchController.class).getProuductByBrand(product.getBrand().toString())).withRel("product-by-brand");
            model.add(link2);

            entityModels.add(model);

        }
        return entityModels;
    }

    @GetMapping("/product/{sku}")
    public EntityModel<Product> getProductBySku(@PathVariable String sku) throws ProductNotFoundException {
        Product product = service.getProductBySku(sku);
        EntityModel<Product> model = new EntityModel<>(product);
        WebMvcLinkBuilder link = linkTo(ControllerLinkBuilder.methodOn(this.getClass()).getAllProducts());
        model.add(link.withRel("all-products"));

        Link link1= linkTo(methodOn(ProductSearchController.class).getProuductByBrand(product.getBrand().toString())).withRel("product-by-brand");
        model.add(link1);
        return model;
    }

    @GetMapping("/product/groupBrand/{brandName}")
    @HystrixCommand(fallbackMethod = "fallBackProduct")
    public List<EntityModel<Product>> getProuductByBrand(@PathVariable String brandName) throws ProductNotFoundException {
        List<Product> products= service.productByBrand(brandName);
        List<EntityModel<Product>> entityModels= new ArrayList<>();
        for(Product product: products){
            EntityModel<Product> model= new EntityModel<>(product);
            WebMvcLinkBuilder link= linkTo(methodOn(ProductSearchController.class).getAllProducts());
            model.add(link.withRel("all-products"));

            Link link1= linkTo(methodOn(ProductSearchController.class).getProductBySku(product.getSku())).withRel("product-by-sku");
            model.add(link1);

            Link link2= linkTo(methodOn(ProductSearchController.class).getProuductByBrand(product.getBrand().toString())).withSelfRel();
            model.add(link2);

            entityModels.add(model);

        }
        return entityModels;
    }

    public ExceptionResponse fallBackProduct() {
        ExceptionResponse resp = new ExceptionResponse();
        resp.setMessage("Server is currently unavailable");
        resp.setDetail(null);
        return resp;
    }


    public ProductSearchService getService() {
        return service;
    }

    public void setService(ProductSearchService service) {
        this.service = service;
    }

}
