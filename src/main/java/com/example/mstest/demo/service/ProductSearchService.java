package com.example.mstest.demo.service;

import com.example.mstest.demo.exception.ProductNotFoundException;
import com.example.mstest.demo.model.Product;

import java.util.List;

public interface ProductSearchService {
    public List<Product> getAllProducts();
    public Product getProductBySku(String sku) throws ProductNotFoundException;
    public List<Product> productByBrand(String brand);
}
