package com.example.mstest.demo.service;

import com.example.mstest.demo.dao.ProductSearchDao;
import com.example.mstest.demo.exception.ProductNotFoundException;
import com.example.mstest.demo.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductSearchServiceImpl implements ProductSearchService {

    @Autowired
    private ProductSearchDao dao;
    @Override
    public List<Product> getAllProducts() {
        return dao.getAllProducts();
    }

    @Override
    public Product getProductBySku(String sku) throws ProductNotFoundException {

        return dao.getProductBySku(sku);
    }

    @Override
    public List<Product> productByBrand(String brand){
        return dao.productByBrand(brand);
    }
}
