package com.example.mstest.demo.model;

import java.math.BigDecimal;
import java.util.Random;

public class Price {
    private double basePrice;
    private double taxablePrice;

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public double getTaxablePrice() {
        return taxablePrice;
    }

    public void setTaxablePrice(double taxablePrice) {
        this.taxablePrice = taxablePrice;
    }

    public Price(double basePrice, double taxablePrice){
        this.basePrice=basePrice;
        this.taxablePrice=taxablePrice;
    }

    public static double getPrice(){
        BigDecimal decimal= new BigDecimal(new Random().nextInt(100000)/100.0).setScale(2, BigDecimal.ROUND_HALF_UP);
        return decimal.doubleValue();
    }
}
