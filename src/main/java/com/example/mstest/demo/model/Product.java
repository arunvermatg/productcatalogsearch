package com.example.mstest.demo.model;


public class Product {
    private static Long itemId=0L;
    private Long id;
    private String name;
    private String sku;
    private String description;
    private String url;
    private Brand brand;
    private ProdColor color;
    private ProductCategory category;

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    private Product(){

    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    private Price price;
    private Seller seller;

    public static Long getItemId() {
        return itemId;
    }

    public static void setItemId(Long itemId) {
        Product.itemId = itemId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public ProdColor getColor() {
        return color;
    }

    public void setColor(ProdColor color) {
        this.color = color;
    }

    public static class Builder{
        private Long id;
        private String name;
        private String sku;
        private String description;
        private String url;
        private Brand brand;
        private ProdColor color;
        private Price price;
        private Seller seller;

       public Builder name(String name){
           this.name=name;
           return this;
       }

        public Builder sku(String sku){
            this.sku=sku;
            return this;
        }

        public Builder description(String description){
            this.description=description;
            return this;
        }

        public Builder url(String url){
            this.url=url;
            return this;
        }
        public Builder brand(Brand brand){
            this.brand=brand;
            return this;
        }


        public Builder price(Price price){
           this.price=price;
           return this;
        }

        public Builder seller(Seller seller){
           this.seller=seller;
           return this;
        }

        public Builder color(ProdColor color){
           this.color=color;
           return this;
        }

        public Product build(){
           Product prod= new Product();
           prod.id=getNextId();
           prod.name=this.name;
           prod.description=this.description;
           prod.url=this.url;
           prod.sku=this.sku;
           prod.brand=this.brand;
           prod.color=this.color;

           return prod;
        }

    }

    private static Long getNextId(){
        synchronized (itemId){
            return itemId++;
        }
    }
}
