package com.example.mstest.demo.model;

public class ProductCategory {
    private String name;
    private CategoryType type;
    private String seller;

    public ProductCategory(String name, CategoryType type, String seller){
        this.name=name;
        this.type=type;
        this.seller=seller;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryType getType() {
        return type;
    }

    public void setType(CategoryType type) {
        this.type = type;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }
}
