package com.example.mstest.demo.model;

public enum ProdColor {
    BLACK, BLUE, GREEN, YELLOW, WHITE, GREY
}
