package com.example.mstest.demo.model;

public enum CategoryType {
    BOTTOM, TOP, GADGET
}
