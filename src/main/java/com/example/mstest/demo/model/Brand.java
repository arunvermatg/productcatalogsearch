package com.example.mstest.demo.model;

public enum Brand {
    VAN_HEUSEN, LEVIS, SHOPPERS, WROGN, WRANGLER, AMERICAN_EAGLE
}
