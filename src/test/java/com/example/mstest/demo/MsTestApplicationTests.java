package com.example.mstest.demo;

import com.example.mstest.demo.dao.ProductSearchDao;
import com.example.mstest.demo.model.Price;
import com.example.mstest.demo.model.Product;
import com.example.mstest.demo.service.ProductSearchService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class MsTestApplicationTests {

	@Test
	void contextLoads() {
	}

	@Mock
	private ProductSearchService serviceMock;

	@Mock
	private ProductSearchDao daoMock;

	@Test
	public void testProducts(){
		List<Product> product= new ArrayList<>();
		Product prod= new Product.Builder().name("abc").build();
		product.add(prod);

		when(serviceMock.getAllProducts()).thenReturn(product);
		assertNotNull(serviceMock.getAllProducts());
	}

	@Test
	public void testArgCapture(){
		ArgumentCaptor<String> capture= ArgumentCaptor.forClass(String.class);
		List<Product> product= new ArrayList<>();
		Product prod= new Product.Builder().name("abc").build();
		double price= Price.getPrice();
		double tax= price*0.12;
		Price prices= new Price(price, tax);

		prod.setPrice(prices);
		product.add(prod);

		daoMock.productByBrand("VAN_HEUSEN");

		verify(daoMock).productByBrand(capture.capture());
		assertEquals(capture.getValue(),"VAN_HEUSEN");
	}

}
